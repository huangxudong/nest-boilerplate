FROM node:alpine

RUN mkdir -p /var/www

COPY . /var/www

WORKDIR /var/www

RUN yarn && \
  yarn build

CMD ["yarn", "start"]
